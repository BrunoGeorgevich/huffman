#ifndef NodeHuff_H
#define NodeHuff_H


class NodeHuff
{
public:

    NodeHuff();
    NodeHuff(int frequency, unsigned char ascii);
    NodeHuff(int frequency, NodeHuff *left, NodeHuff *right);

    ~NodeHuff();


    int frequency() const;
    void setFrequency(int frequency);

    NodeHuff *left() const;
    void setLeft(NodeHuff *left);

    NodeHuff *right() const;
    void setRight(NodeHuff *right);

    unsigned char ascii() const;
    void setAscii(unsigned char ascii);
    bool isLeaf();

private:

    /*Acesso aos filhos da direita e da esquerda*/
    NodeHuff *m_left;
    NodeHuff *m_right;

    /*Obtem a frenquência do caracter*/
    int m_frequency;
    /*Representa o caracter*/
    unsigned char m_ascii;


};

#endif // NodeHuff_H
