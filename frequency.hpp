#ifndef FREQUENCY_H
#define FREQUENCY_H

#include <QByteArray>
#include <QDebug>
#include <QFile>

#include <iostream>

using namespace std;

/*
 *
 * Essa classe se responsibilizará pela leitura e contagem da frequencia
 * de aparição de bytes iguais em um arquivo .huff
 *
 */

class Frequency
{
public:

    /* Esse primeiro método é estático, ou seja,
     * independe de uma instância!
     *
     * Ele é responsável por extrair a matriz de frequencias
     * do char* passado para ele!
     */
    static QHash<unsigned char,int> getFrequencyHash(QString path);

    /*
     * Esse próximo método utiliza de template,
     * foi um conceito que não foi muito bem aplicado nesse método,
     * mas é interessante que você aprenda a utilizar do mesmo.
     * Portanto, o apliquei aqui mais para que vocẽ possa aprender
     * a usá-lo!
     *
     * Métodos que usam template devem ter seu corpo no .h!
     */
    template<typename E>
    static void printArray(E *file, int size){

        for(int i = 0; i < size; i++) {
            cout << file[i] << " ";
        }

        cout << endl;
    }
};

#endif // FREQUENCY_H
