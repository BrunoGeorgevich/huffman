#ifndef DECODINGHUFF_H
#define DECODINGHUFF_H

#include "treehuff.hpp"

#include <QByteArray>
#include <QBitArray>
#include <QString>
#include <QDebug>
#include <QFile>


class decodinghuff
{
public:
    decodinghuff();

    void unmountingFile(QString path);
    void searchChar(NodeHuff *root);
    QBitArray byteArrayToBitArray(QByteArray byte);
    int bitToInt(QBitArray bit, int size, int pos);
    QString treeLeftSide(QString treeCode);
    QString treeRightSide(QString treeCode);
    NodeHuff* reassemblingTree(QString treeCode);
    QByteArray decodification(QByteArray fileByte, NodeHuff *root);


private:

    qint64 m_index;
    int m_trashSize;
    int m_nameFileSize;
    int m_codeTreeSize;
    QString m_fileName;
    QString m_treeCode;
    QBitArray m_fileBit;
    QByteArray m_fileByte;
    NodeHuff *m_huffmanRoot;

};

#endif // DECODINGHUFF_H
