#ifndef TreeHuff_H
#define TreeHuff_H

#include <QList>
#include <QHash>

#include <iostream>

#include "nodehuff.hpp"

using namespace std;

class TreeHuff
{
public:

    TreeHuff();
    ~TreeHuff();

    /*Cria os nós da árvore de huffman*/
    QList<NodeHuff *> creatingNodes(QHash<unsigned char, int> frequency);

    /*Ordena a lista de nodes*/
    QList<NodeHuff *> sortTree(QList<NodeHuff *> list);

    /*Insere nodes já ordenados na lista*/
    QList<NodeHuff *> insertSortNode(NodeHuff *&node, QList<NodeHuff *> &list);

    /*Monta a aŕvore de huffman*/
    NodeHuff *createTree(QList<NodeHuff *> &list);

    /*Printa a árvore de Huffman*/
    NodeHuff *printTree(NodeHuff *rootNode, int spaces, int padding);  
};

#endif // TreeHuff_H
