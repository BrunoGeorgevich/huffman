#include "filereader.hpp"
#include <QDebug>

FileReader::FileReader(QString path)
{
    /*Faz s leitura do arquivo e passa para binário*/
    m_file = new fstream(qPrintable(path), ios::in|ios::binary|ios::ate);
    m_sizeOfFile = m_file->tellg();
    m_file->seekg(0, ios::beg);
}

/*Faz a leitura do arquivo por uma quantidade de bytes definido pelo parâmetro size*/
unsigned char *FileReader::readFile(int size)
{
    unsigned char *buffer;

    if(size > 0)
        buffer = new unsigned char[m_sizeOfFile];
    else
        cout << "ERRO LEITURA ARQUIVO" << endl;


    if(m_file){
        for(int i = 0; !m_file->eof(); ++i)
        {
            /*Cast utilizado para não haver problemas de conversão na leitura do char*/
            m_file->read((char*)&buffer[i*size],size);
        }
        return buffer;
    }

    return 0;
}

/*Retorna o tamanho do arquivo*/
int FileReader::sizeOfFile() const
{
    return m_sizeOfFile;
}

void FileReader::compressingFile(string* codeList)
{
    /*Testando a compressão pela codificação por texto*/
    ofstream huffFile(qPrintable(QDir::homePath() + "/Área de Trabalho/file.huff"), ios::out);

    unsigned char bufferCompress[800];

    do{
        m_file->read((char*)&bufferCompress[0],800);

//        for(int i = 0; i < 800; ++i){
//            for(int j = 0; j < codeList[bufferCompress[i]].length(); ++j){
//                if(codeList[bufferCompress[i]].at(j) == '1')
//                {

//                }else if(codeList[bufferCompress[i]] == '0')
//                {

//                }

//            }
//        }




      }while(!m_file->eof());
}



