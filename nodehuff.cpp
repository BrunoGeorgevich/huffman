#include "nodehuff.hpp"

NodeHuff::NodeHuff()
{
    m_left = nullptr;
    m_right = nullptr;

    m_frequency = 0;
    m_ascii = 0;
}

NodeHuff::NodeHuff(int frequency, unsigned char ascii)
{
    m_left = nullptr;
    m_right = nullptr;

    m_frequency = frequency;
    m_ascii = ascii;
}

NodeHuff::NodeHuff(int frequency, NodeHuff *left, NodeHuff *right)
{
    m_left = left;
    m_right = right;
    m_frequency = frequency;
    m_ascii = 0;
}

NodeHuff::~NodeHuff()
{

}

int NodeHuff::frequency() const
{
    return m_frequency;
}

void NodeHuff::setFrequency(int frequency)
{
    m_frequency = frequency;
}

NodeHuff *NodeHuff::left() const
{
    return m_left;
}

unsigned char NodeHuff::ascii() const
{
    return m_ascii;
}

void NodeHuff::setAscii(unsigned char ascii)
{
    m_ascii = ascii;
}

void NodeHuff::setLeft(NodeHuff *left)
{
    m_left = left;
}

NodeHuff *NodeHuff::right() const
{
    return m_right;
}

void NodeHuff::setRight(NodeHuff *right)
{
    m_right = right;
}

bool NodeHuff::isLeaf()
{
    if(m_left == nullptr && m_right == nullptr)
    {
        return true;
    }else{
        return false;
    }
}




