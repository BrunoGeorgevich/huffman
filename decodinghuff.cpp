#include "decodinghuff.hpp"

decodinghuff::decodinghuff()
{

}

void decodinghuff::unmountingFile(QString path)
{
    QFile fileHuff(path);
    QByteArray allFile;

    if (!fileHuff.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

    allFile.append(fileHuff.readAll());

    QBitArray header(byteArrayToBitArray(allFile.mid(0,3)));

    m_trashSize = bitToInt(header, 3, 0);
    m_codeTreeSize = bitToInt(header, 13, 3);
    m_nameFileSize = bitToInt(header, 8, 16);

    m_fileName = allFile.mid(3,m_nameFileSize);
    m_treeCode = allFile.mid(3 + m_nameFileSize, m_codeTreeSize);

    m_huffmanRoot = reassemblingTree(m_treeCode);

    QFile file("../Huffman/test/" + m_fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        qDebug() << "The .huff is not open";
         return;
    }

    qDebug()<< 3 + m_nameFileSize+ m_codeTreeSize << ":: POSITION";
    qDebug()<< m_nameFileSize << "NAME";
    qDebug()<< m_codeTreeSize << "TREE";

    file.write(decodification(allFile.mid(3 + m_nameFileSize+ m_codeTreeSize), m_huffmanRoot));

    file.close();
}

QByteArray decodinghuff::decodification(QByteArray fileByte, NodeHuff *root)
{
   m_index = 0;
   m_fileBit.operator =(byteArrayToBitArray(fileByte));
   qDebug()<<  m_fileBit.size();
   m_fileBit.resize(m_fileBit.size() - m_trashSize);

   qDebug()<<  m_fileBit.size();

   for(int i = 0; i < m_fileBit.size(); ++i){
       if (!m_fileBit.at(i)) {
           root = root->left();
           if(root->isLeaf()){
               m_fileByte.append(root->ascii());
               root = m_huffmanRoot;
           }
//           qDebug()<< "ENTROU AQUI ESQUERDA !!!" << i;
       }

       if (m_fileBit.at(i)) {
           root = root->right();
           if(root->isLeaf()){
               m_fileByte.append(root->ascii());
               root = m_huffmanRoot;
           }
//           qDebug()<< "ENTROU AQUI ---------->>>>> DIREITA !!!" << i;
       }


   }


   return  m_fileByte;
}




QBitArray decodinghuff:: byteArrayToBitArray(QByteArray byte)
{
    // Create a bit array of the appropriate size
    QBitArray bit(byte.size()*8);

    qDebug()<< "SIZE QBitArray ::"<<byte.count()*8;

    // Convert from QByteArray to QBitArray
    for(int i=0; i<byte.size(); ++i){
        for(int b=0; b<8;b++) {
            bit.setBit( i*8+b, byte.at(i)&(1<<(7-b)) );
        }
    }

    return bit;
}

int decodinghuff::bitToInt(QBitArray bit, int size, int pos)
{
    QString bin = "";
    int integer = 0;
    int aux = 2;


    for(int i = pos, j = 0; i < (size + pos); i++, j++){
        if(bit.at(i))
        {
            integer += pow(aux,(size - (1+j)));
        }
    }

    return integer;
}

QString decodinghuff::treeLeftSide(QString treeCode)
{
    QString leftTree;
    QList<char> verification;
    if(treeCode.at(0).toLatin1()=='*' && treeCode.at(1).toLatin1()=='*')
    {
        verification.push_back('*');
        for(int i=1; i<treeCode.size();i++)
        {
            if(treeCode.at(i).toLatin1()=='*' && treeCode.at(i-1).toLatin1()!='!')
            {
                verification.push_back('*');
            }
            else
            {
                if(treeCode.at(i).toLatin1()!='!')
                    verification.pop_back();
            }
            leftTree.append(treeCode.at(i));
            if(verification.isEmpty())
            {
                break;
            }
        }
    }
    else
    {
        leftTree=treeCode.at(1);
        if(treeCode.at(1).toLatin1()=='!')
        {
            leftTree=treeCode.at(2);
        }
    }
    return leftTree;
}

QString decodinghuff::treeRightSide(QString treeCode)
{
    QString treeLeftCode=treeLeftSide(treeCode);
    if(treeLeftCode.size()==1)
    {
        if(treeLeftCode.at(0).toLatin1()=='*' || treeLeftCode.at(0).toLatin1()=='!')
            treeCode.remove(1,treeLeftCode.size());
    }
    treeCode.remove(1,treeLeftCode.size());
    QString treeRightCode=treeLeftSide(treeCode);
    return treeRightCode;
}

NodeHuff* decodinghuff::reassemblingTree(QString treeCode)
{
    NodeHuff* left;
    NodeHuff* right;
    QString treeLeft=treeLeftSide(treeCode);
    QString treeRight=treeRightSide(treeCode);
    if(treeLeft.size()>1)
    {
        left = reassemblingTree(treeLeft);
    }
    else
    {
        left=new NodeHuff(0,treeLeft.at(0).toLatin1());
    }
    if(treeRight.size()>1)
    {
        right = reassemblingTree(treeRight);
    }
    else
    {
        right = new NodeHuff(0,treeRight.at(0).toLatin1());
    }
    return new NodeHuff(0,left,right);
}
