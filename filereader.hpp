#ifndef FILEREADER_H
#define FILEREADER_H

#include <iostream>
#include <fstream>

#include <QString>
#include <QDir>

using namespace std;

/*
 *
 * Essa classe se responsibilizará pela leitura do arquivo que deseja comprimir
 * e escrita do arquivo comprimido (.huff)
 *
 */

class FileReader
{

public:

    /*
    * O construtor recebe um qstring path que
    * é o exato caminho para o arquivo que deseja
    * ler!
    */
    FileReader(QString path);

     /*
     *Esse método é capaz de ler o arquivo e continuar de
     *onde ele parou a leitura, bastanto apenas preencher o buffer
     *com a quantidade de bytes desejada
     */
    unsigned char *readFile(int size);
    int sizeOfFile() const;
    void setSizeOfFile(int sizeOfFile);
    void testNewFile();
    void compressingFile(string* codeList);

private:


    //Variável privada responsável pela leitura do arquivo
    fstream *m_file;
    long int m_sizeOfFile;


};

#endif // FILEREADER_H
