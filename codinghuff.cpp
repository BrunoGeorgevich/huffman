#include "codinghuff.hpp"

CodingHuff::CodingHuff(){}

CodingHuff::~CodingHuff(){}

/* Monta um QString com a codificação de Huffman do arquivo.*/
QString CodingHuff::codify(QByteArray byteA)
{
    QString buffer = "";
    QString encodedString = "";

    int trashSize = 0;

    for(int i = 0; i < byteA.size(); i++) {
        buffer += m_encoding.value((int)byteA[i]);
    }

    if(buffer.size() < 1024 && (buffer.size()%8) != 0) {
        trashSize = 8 - (buffer.size() % 8);
    }

    m_trashSize = trashSize;

    while(trashSize != 0) {
        buffer.append("0");
        trashSize--;
    }

    return buffer;

    /*
     * O que falta fazer:
     *  - (FEITO)Gerar uma codificação para a árvore com um caractér especial;
     *  - (FEITO)Pegar o tamanho da codificação da árvore;
     *  - (FEITO)Pegar o nome do arquivo sucedido de sua extensão;
     *  - (FEITO)Pegar o tamanho do nome do arquivo;
     *  - (FEITO)Salvar em um arquivo .huff;
     */
}

/* Gera a codificação de Huffman buscando as folhas na arvore*/
void CodingHuff::leaves(NodeHuff *root)
{
    if (root->left() == NULL && root->right() == NULL) {
        int ascii = (int)root->ascii();
        m_encoding.insert(ascii,m_codification);
    }
    else {
        if (root->left() != NULL) {
            m_codification.push_back('0');
            leaves(root->left());
        }

        m_codification.resize(m_codification.size() - 1);

        if (root->right() != NULL) {
            m_codification.push_back('1');
            leaves(root->right());
        }

        m_codification.resize(m_codification.size() - 1);
    }
}

/* Gera a codificação da árvore de Huffman buscando as folhas na arvore*/

void CodingHuff::codingTree(NodeHuff *root)
{
    if (root->left() == NULL && root->right() == NULL) {

        m_codingTree.resize(m_codingTree.size() - 1);
        QString ascii = (QString)root->ascii();
        if(ascii == "*" || ascii == "!"){
            m_codingTree.push_back("!");
            m_codingTree.push_back(ascii);
        }else{
            m_codingTree.push_back(ascii);
        }
    }
    else {

        if (root->left() != NULL) {
            m_codingTree.push_back('*');
            codingTree(root->left());
        }

        if (root->right() != NULL) {
           m_codingTree.push_back('*');
            codingTree(root->right());
        }
    }
}

/*Passa de decimal para binário*/
QString CodingHuff::decimalToBinary(int decimal)
{
    QString binary;

    do{
        if((int)decimal%2){
            binary.push_front('1');
        }else{
            binary.push_front('0');
        }

        decimal/= 2;

    }while(decimal>=1);

    return binary;
}

/*Montando o arquivo*/
void CodingHuff::ridingTheFile(QString compactCode)
{
   QString fileNameSizeBin(decimalToBinary(m_fileName.size()));
   fileNameSizeBin = fileNameSizeBin.rightJustified(8,'0');

   QString trashSizeBin(decimalToBinary(m_trashSize));
   trashSizeBin = trashSizeBin.rightJustified(3, '0');

   QString treeSizeBin(decimalToBinary(m_codingTree.size()));
   treeSizeBin = treeSizeBin.rightJustified(13, '0');

   QString header = trashSizeBin + treeSizeBin + fileNameSizeBin;

   QBitArray bitHuff = byteToBite(header);

   /*Montando o arquivo .huff*/
   QFile fileHuff(m_pathOfFile + m_fileName.section('.',0,0) + ".huff");
   if (!fileHuff.open(QIODevice::WriteOnly | QIODevice::Append))
   {
       qDebug() << "The .huff is not open";
        return;
   }

   /*Montando o cabeçalho do .huff*/
   QByteArray byteHuff;

   byteHuff.operator +=(writingAByte(bitHuff));
   byteHuff.operator +=(m_fileName+m_codingTree);

   fileHuff.write(byteHuff);
   byteHuff.clear();

   for(int i = -1; 1024*i < compactCode.size(); ){
      ++i;
      byteHuff.operator +=(writingAByte(byteToBite(compactCode.mid(1024*i,1024))));
      fileHuff.write(byteHuff);
      byteHuff.clear();
   }

   fileHuff.close();

}

QByteArray CodingHuff::writingAByte(QBitArray bit)
{
    QByteArray byteArray;
    char byte;

        for(int i = 0; i < bit.size(); ++i){
            char masc = true;
            if(bit.at(i))
            {
                byte |= masc << (7-i%8);
            }

            if(i%8 == 7){
                byteArray.append(byte);
                byte = false;
            }

       }




    return byteArray;
}

void CodingHuff::getPathOfFile(QString path)
{
   m_pathOfFile = path.section('/',0,-2) + "/";
   m_fileName = path.section('/',-1);
}

/*Passa de byte para bit*/
QBitArray CodingHuff::byteToBite(QString byte)
{
    QBitArray bite(byte.size());

    for(int i = 0; i < byte.size(); i++){
        if(byte.at(i) == '1'){
          bite.setBit(i, true);
        }else{
           bite.setBit(i, false);
        }
    }

    return bite;

}

QString CodingHuff::getCodingTree()
{
    return m_codingTree;
}


QHash<int, QString> CodingHuff::encoding()
{
    return m_encoding;
}

QString CodingHuff::getEncoding(int key)
{
       return m_encoding.value(key);
}

int CodingHuff::hashSize()
{
    return m_encoding.size();
}
