#-------------------------------------------------
#
# Project created by QtCreator 2015-05-11T20:29:33
#
#-------------------------------------------------

#Obs.:  Foi adicionado para aceitar nullptr.
QMAKE_CXXFLAGS += -std=c++0x

QT       += core

QT       -= gui

TARGET = Luccas_Huffman
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    filereader.cpp \
    frequency.cpp \
    treehuff.cpp \
    nodehuff.cpp \
    codinghuff.cpp \
    decodinghuff.cpp

HEADERS += \
    decodinghuff.hpp \
    codinghuff.hpp \
    filereader.hpp \
    frequency.hpp \
    nodehuff.hpp \
    treehuff.hpp
