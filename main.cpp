#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QHash>
#include <QString>
/*TESTE*/
#include <QBitArray>

#include "filereader.hpp"
#include "frequency.hpp"
#include "nodehuff.hpp"
#include "treehuff.hpp"
#include "codinghuff.hpp"
#include "decodinghuff.hpp"

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString path = "../Huffman/assets/0.txt";

    /*Contando as frenquencias dos caracteres, gera um hash com caracteres e frequâncias*/
    QHash<unsigned char,int> frequencyHash = Frequency::getFrequencyHash(path);

    /*Inicia o objeto para a criação da árvore*/
    TreeHuff huffmanTree;

    /*
     *Um objeto QLista  que recebe uma lista ordenada de NodeHuff
     *gerada através dos dados do frequencyHash[].
     */

    QList<NodeHuff *> nodeArray = huffmanTree.creatingNodes(frequencyHash);

    NodeHuff *huffmanRoot;
    CodingHuff huffmanEncoding;
    huffmanEncoding.getPathOfFile(path);

    /*
     *Um NodeHuff que representa uma raiz que recebe uma árvore gerada
     *através dos dados da lista ordenada de NodeHuff, nodeArray[]
     */

    huffmanRoot = huffmanTree.createTree(nodeArray);

    huffmanEncoding.leaves(huffmanRoot);
    huffmanEncoding.codingTree(huffmanRoot);

    /*TESTE*/

    qDebug()<< huffmanEncoding.hashSize();
    qDebug()<< huffmanEncoding.getEncoding((int)'a');
    qDebug()<< huffmanEncoding.getEncoding((int)'b');
    qDebug()<< huffmanEncoding.getEncoding((int)'c');
    qDebug()<< huffmanEncoding.getEncoding((int)'d');
    qDebug()<< huffmanEncoding.getEncoding((int)'e');
    qDebug()<< huffmanEncoding.getEncoding((int)'f');


 qDebug()<<"codificacao da arvore::"<< huffmanEncoding.getCodingTree();

    /*Criando o arquivo .huff*/

    QFile file(path);
    file.open(QIODevice::ReadOnly);
    QString compactCode = "";

    while(!file.atEnd()){
        compactCode += huffmanEncoding.codify(file.read(1024));
    }

    qDebug()<< compactCode << "LLLLLLLLLL" << compactCode.size();

    huffmanEncoding.ridingTheFile(compactCode);

    /* TESTANDO */

    QBitArray testando(24,false);

    testando.setBit(0,true);
    testando.setBit(4,true);
    testando.setBit(6,true);
    testando.setBit(13,true);
    testando.setBit(15,true);
    testando.setBit(16,true);
    testando.setBit(23,true);

    decodinghuff test;

    test.unmountingFile("../Huffman/assets/0.huff");

    return  0;
}


