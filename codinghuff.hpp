#ifndef CODINGHUFF_H
#define CODINGHUFF_H

#include <QBitArray>
#include <QString>
#include <QDebug>
#include <QMap>

#include <iostream>
#include <QFile>

#include "nodehuff.hpp"

using namespace std;


class CodingHuff
{
public:
    CodingHuff();
    ~CodingHuff();

    QString codify(QByteArray byteA);
    void leaves(NodeHuff *node);
    void getCoding(NodeHuff *root);
    void getPathOfFile(QString path);
    void codingTree(NodeHuff *root);
    void ridingTheFile(QString compactCode);
    QString getCodingTree();
    QString getEncoding(int key);
    QString decimalToBinary(int decimal );
    QBitArray byteToBite(QString byte);
    QHash<int,QString> encoding();
    QByteArray writingAByte(QBitArray bit);
    int hashSize();

private:

    int m_count = 0;
    QString m_codification;
    QString m_codingTree = "*";
    QString m_fileName;
    QString m_pathOfFile;
    QHash<int,QString> m_encoding;

    int m_trashSize;
};

#endif // CODINGHUFF_H
