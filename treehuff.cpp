#include "treehuff.hpp"
#include <list>
#include <cstdio>

using namespace std;

TreeHuff::TreeHuff()
{

}

TreeHuff::~TreeHuff()
{

}


/*Cria nós e os coloca em uma QList*/
QList<NodeHuff *> TreeHuff::creatingNodes(QHash<unsigned char,int> frequency)
{
    QList<NodeHuff *> nodes;

    foreach(unsigned char c, frequency.keys()) {
        nodes.append(new NodeHuff(frequency[c],(int)c));
    }

    nodes = sortTree(nodes);

    return nodes;
}

/*Ordena a QList do menor para o maior*/
QList<NodeHuff *> TreeHuff::sortTree(QList<NodeHuff *> list)
{
    QList<NodeHuff *> newList;


    while(list.size()) {

        NodeHuff *maxNode = new NodeHuff(0,0);
        int j = 0;

        for(int i = 0; i < list.size(); i++) {
            if(list.at(i)->frequency() > maxNode->frequency()) {
                maxNode = list.at(i);
                j = i;
            }
        }

        newList.insert(0,maxNode);
        list.removeAt(j);
    }

    return newList;
}

QList<NodeHuff *> TreeHuff::insertSortNode(NodeHuff *&node, QList<NodeHuff *> &list)
{
    for(int i = 0; i < list.size(); i++){

        if(list.at(i)->frequency() > node->frequency()){
            list.insert( i, node);
            break;
        }else if(i == list.size() - 1){
            list.append(node);
            break;
        }

    }

    return list;
}

NodeHuff *TreeHuff::createTree(QList<NodeHuff *> &list)
{
    if(!list.size())
        return NULL;

    NodeHuff *newNode = NULL;

    while (list.size() > 1) {
        newNode = new NodeHuff(list.at(0)->frequency() + list.at(1)->frequency(), list.at(0), list.at(1));

        list.removeFirst();
        list.removeFirst();

        list = insertSortNode(newNode, list);
    }

    /*Retorna a raiz da HuffmanTree*/
    return newNode;
}
