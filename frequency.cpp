#include "frequency.hpp"

QHash<unsigned char, int> Frequency::getFrequencyHash(QString path)
{
    QFile f(path);
    f.open(QIODevice::ReadOnly);

    QHash<unsigned char,int> frequencyHash;

    while(!f.atEnd()) {

        QByteArray buff = f.read(1024);

        for(int i = 0; i < buff.size(); i++) {
            /*frequencyHash.value(buff[i]) retorna o valor da key buff[i]*/
            frequencyHash.insert(buff[i],frequencyHash.value(buff[i]) + 1);
        }
    }

    f.close();

    return frequencyHash;
}
